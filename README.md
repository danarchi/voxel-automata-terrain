# Voxel Automata Terrain #


![An Example.](https://bytebucket.org/BWerness/voxel-automata-terrain/raw/43b1ab2e461406bd7841bf4d49a3ea61a17019ba/Examples/XmfrLEtJ2AUTxSYbH592WuMdqo1-513-8890.png)

This produces and renders voxel terrain based on a merging of the ideas of the square-diamond algorithm for noise generation with cellular automata.

I hope to add an extended description soon, but so many people seemed interested in playing with this that
I wanted to post the code first, and make it readable later.

The basic idea is that you pretend you've drawn a certain level of detail of the image into a grid of voxels and you are now wondering how to make a twice as detailed image.
You do so with a collection of rules.  First, you have a rule for how to fill in the center of a cube when you know all the corners, second you have a rule 
for filling in all the faces when you know the corners and centers, and then finally you have a rule for filling in the edges when you know everything you've
filled in so far.  In this way, you can go from a voxel grid to one twice the size.  Repeating this many times gives you a large detailed voxel grid.  

Changing the rules can give you a wild diversity of different images!

### A Warning ###

While I plan to continue to polish this and hope to one day turn this into something more usable, this is currently in what us academics call "research code."
It might crash, the controls are rudimentary, and compatibility is untested.  It currently exists to allow the exploration of the concept, but has *no* polish!

### How to run? ###

This is a [Processing](https://processing.org/) project, so you should start by downloading and installing Processing.  You then need to download the .pde file and the folder called "data" into a folder called "ThreeState3DBitbucket".  You can then open and run this in Processing.
You do *not* need to download the "Examples" to run it, but it contains a large number of example output images, and the files needed to load and explore these spaces yourself!

### Controls ###

* **Click and drag mouse** - This rotates the camera view.
* **w** - Move forward.
* **a** - Move left.
* **s** - Move back.
* **d** - Move right.
* **q** - Move up.
* **z** - Move down.
* **Shift** - Double movement speed.
* **Tab** - Toggle constant forward movement.
* **1-9** - Set the size of the voxel grid to be 2^1 through 2^9 and randomize the initial conditions.
* **x** - Cut the voxel grid in half keeping the same initial conditions.
* **c** - Double the world size extending the current initial conditions.
* **y** - Save a png which contains all the information needed to reload this world.  *Do not change the name or recompress this png, it will break it and you will never find this place again!*
* **e** - Save a large image of the current view.
* **l** - Load a png that ends in "SVI" that you saved by pressing "y".
* **r** - Create a random rule.
* **,** - Decrease density of non-empty cells in the random rule.
* **.** - Increase density of non-empty cells in the random rule.
* **i** - Create random rule where it tries to make cells similar to their neighbors.
* **[** - Make the random rule try to be more similar to neighbors (more solid colored clumps).
* **]** - Make the random rule try to be more similar to neighbors (more random colored clumps).
* **{** - Make the density higher when making a rule with "i".
* **}** - Make the density lower when making a rule with "i".
* **b** - Toggle drawing the skysphere.
* **v** - Toggle drawing a viewfinder.

### Help it doesn't work! ###

I currently only have access to my personal laptop, so I only know that this runs on a 5 year old retina MacBook Pro.  If you are having difficulty running, you can try contacting me through Twitter: [@R4_Unit](https://twitter.com/R4_Unit)